﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BookLibraryFinal.Models
{
    public class Genre
    {
        public int GenreId { get; set; }

       /* [Required]
        [StringLength(100, MinimumLength = 1)]*/ //reQ field + lenght
        public String GenreName { get; set; }

        public virtual ICollection<Book> Genres { get; set; }
    }
}