﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BookLibraryFinal.Models
{
    public class Book
    {
        public int BookId { get; set; }

        /*[Required]
        [StringLength(300, MinimumLength = 1)]*/
        public String Title { get; set; }

        [Required]
        public DateTime ReleaseDate { get; set; }

        //[Display(Name = "Author")]
        public int AuthorId { get; set; }

        //[Display(Name = "Genre")]
        public int GenreId { get; set; }

        //[StringLength(500, MinimumLength = 1)]
        public String Description { get; set; }


        public virtual Author Author { get; set; }
        public virtual Genre Genre { get; set; }
    }
}