﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BookLibraryFinal.Models
{
    public class Author
    {
        public int AuthorId { get; set; }

       /* [Required]
        [StringLength(200, MinimumLength = 1)]*/
        public String FirstName { get; set; }

     
        //[StringLength(200, MinimumLength = 1)]  
        public String LastName { get; set; }

        public virtual ICollection<Book> Authors { get; set; }
    }
}