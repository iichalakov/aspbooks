﻿using BookLibraryFinal.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BookLibraryFinal.DataBase
{
    public class BookDbContext : DbContext
    {

        public DbSet<Models.Book> Books { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<Genre> Genres { get; set; }
    }
}